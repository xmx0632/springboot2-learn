package cn.xmx0632.admin.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dax
 */
@SpringBootApplication
public class DemoClientApplication {

    public static void main(String[] args) {

        SpringApplication.run(DemoClientApplication.class, args);
    }

}
