package cn.xmx0632.admin.client;

import lombok.*;

import java.io.UnsupportedEncodingException;

/**
 * @author xmx0632
 */
//@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class LombokTest {

    private final int finalVal = 10;

    private String name;

    // builder 构建对象时设置默认值
    @Builder.Default
    private int age = 99;

    private boolean old;

    @SneakyThrows(UnsupportedEncodingException.class)
    public static void main(String[] args) {
        LombokTest test = new LombokTest();
        System.out.println(test.getAge());


        LombokTest test1 =  LombokTest.builder().build();
        System.out.println(test1.getAge());

        int ii = 100;
//        int vv = ii / 0;

        throw new UnsupportedEncodingException("aaaa");
//        Exception in thread "main" java.lang.ArithmeticException: / by zero
//        at cn.xmx0632.admin.client.LombokTest.main(LombokTest.java:17)

    }

}
