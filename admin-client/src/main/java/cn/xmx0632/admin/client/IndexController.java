package cn.xmx0632.admin.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmx0632
 */
@RestController
@Slf4j
public class IndexController {

    @GetMapping(path = "/demo", produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody
    String index() {
        log.error("readme");
        log.warn("readme");
        log.info("readme");
        log.debug("readme");

        StringBuilder sb = new StringBuilder(30);
        if (log.isDebugEnabled()){
            sb.append("debug").append(" ");
        }
        if (log.isInfoEnabled()){
            sb.append("info").append(" ");
        }
        if (log.isWarnEnabled()){
            sb.append("warn").append(" ");
        }
        if (log.isErrorEnabled()){
            sb.append("error").append(" ");
        }
        return sb.toString();
    }


    @GetMapping(path = "/health.json", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody
    String health() {
        log.error("health check,up up");
        log.warn("health check");
        log.info("health check");
        log.debug("health check");

        return "{ \"status\" : \"UP\" }";
    }
}
