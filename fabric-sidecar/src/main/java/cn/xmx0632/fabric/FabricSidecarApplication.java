package cn.xmx0632.fabric;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

@EnableSidecar
@EnableDiscoveryClient
@SpringBootApplication
public class FabricSidecarApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabricSidecarApplication.class, args);
	}

}

