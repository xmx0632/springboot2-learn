package cn.xmx0632.admin.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RestController
public class IndexController {

    private AtomicLong index = new AtomicLong(0);

    @RequestMapping("/hello")
    public String hello(String name){
        long currentIndex = index.addAndGet(1);
        System.out.println("this is :" + name + " index:" + currentIndex);
        log.info("this is :{} , {}" , name, currentIndex);
        return "hi " + name;
    }

    @RequestMapping("/timeout")
    public String timeout(long t){
        try{
            System.out.println("sleep ...1:" + t );
            //睡5秒，网关Hystrix3秒超时
            Thread.sleep(t);
            System.out.println("sleep ...2:" + t);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "timeout";
    }
}
