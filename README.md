# 使用spring boot admin监控进程服务
# admin server

http://localhost:8888/
      
      name: server
      password: serverpassword
      
# admin client
访问url，测试打印日志

http://localhost:8081/demo
      
      name: client
      password: clientpassword

spring.profiles.active=dev|prod

#### 可以动态调整日志级别

### 添加注册中心(eureka)
    http://localhost:10025/eureka/
    
    http://eureka:eureka123@localhost:10025/eureka/
    
    prod模式，设置hosts
    #set hosts
    #127.0.0.1 e1
    #127.0.0.1 e2
    #127.0.0.1 e3
    
    动态拉出某个服务实例 UP-> OUT_OF_SERVICE（需要等2分钟）：
    PUT http://localhost:10025/eureka/apps/CONSUMER-SERVICE/192.168.8.189:18899-consumer-service/status?value=OUT_OF_SERVICE
    curl --location --request PUT 'http://localhost:10025/eureka/apps/CONSUMER-SERVICE/192.168.8.189:18899-consumer-service/status?value=OUT_OF_SERVICE'
    
    动态拉入某个实例（如果新服务发布失败，则恢复老服务）：
    DELETE http://localhost:10025/eureka/apps/CONSUMER-SERVICE/192.168.8.189:18899-consumer-service/status
    curl --location --request DELETE 'http://localhost:10025/eureka/apps/CONSUMER-SERVICE/192.168.8.189:18899-consumer-service/status'



### 使用spring boot gateway控制接口访问
    接口调用
    http://localhost:9999/demo/hello?name=ded
    调用网关接口超时，返回网关超时错误消息
    http://localhost:9999/demo/timeout?t=5000
    
    限流监控gateway hystrix.stream:
	http://localhost:9999/actuator/hystrix.stream
	

### turbin:
    限流信息聚合
	http://localhost:9000/turbine.stream

    http://localhost:9000/turbine.stream?cluster=gateway-service
	
### 后台接口服务(consumer-service)
    http://localhost:9700/timeout?name=uuu
    超时测试
	http://localhost:9700/timeout?t=5000
    
### 查看gateway接口流量(hystrix dashboard)
	http://localhost:9090/hystrix
    

# TODO 
### 重构pom依赖，去掉重复配置
### gateway中访问auth接口服务，对api进行访问控制判断和鉴权
### gateway中对接口做限流、熔断配置
### gateway集成apollo动态修改限流配置

### 增加spring cloud config


